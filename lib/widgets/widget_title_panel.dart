import 'package:flutter/material.dart';

class TitlePanelWidget extends StatelessWidget {
  final String title;
  final List<String> time;

  TitlePanelWidget(this.title, this.time);

  @override
  Widget build(BuildContext context) {
    int timeListLength = this.time.length;
    return Container(
      width: double.infinity,
      padding: EdgeInsets.all(10.0),
      child: Column(
        children: <Widget>[
          Text(
            '${this.title != null ? this.title : ""}Report',
            style: TextStyle(fontSize: 28.0, fontWeight: FontWeight.bold),
            textAlign: TextAlign.center,
          ),
          Text(
            'as of',
            style: TextStyle(fontSize: 12.0),
            textAlign: TextAlign.center,
          ),
          Text(
            '${timeListLength > 0 ? this.time[0] : ""} | ${timeListLength > 0 ? this.time[1] : ""} ${timeListLength > 2 ? this.time[2] : ""}',
            style: TextStyle(fontSize: 16.0),
            textAlign: TextAlign.center,
          )
        ],
      ),
    );
  }
}
