import 'package:covidreport/models/enum_covid_colors.dart';
import 'package:flutter/material.dart';

class ProfileItemWidget extends StatelessWidget {
  final String title;
  final String count;
  final CovidColors countColor;

  ProfileItemWidget({this.title, this.count, this.countColor});

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(vertical: 10.0, horizontal: 20.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Container(
            margin: EdgeInsets.only(right: 5.0),
            child: Text(
              this.title,
              style: TextStyle(fontSize: 18.0),
            ),
          ),
          Container(
            margin: EdgeInsets.only(left: 5.0),
            padding: EdgeInsets.symmetric(horizontal: 10.0, vertical: 5.0),
            decoration: BoxDecoration(
              color: Colors.indigoAccent,
              shape: BoxShape.rectangle,
              borderRadius: BorderRadius.all(Radius.elliptical(10, 10)),
            ),
            child: Stack(
              children: <Widget>[
                Text(
                  count,
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    foreground: Paint()
                      ..style = PaintingStyle.stroke
                      ..strokeWidth = 0.8
                      ..color = Colors.white,
                    fontWeight: FontWeight.bold,
                    fontSize: 22.0,
                  ),
                ),
                Text(
                  count,
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    color: getColor(this.countColor),
                    fontSize: 22.0,
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  Color getColor(CovidColors color) {
    switch (color) {
      case CovidColors.red:
        return Colors.redAccent;
      case CovidColors.green:
        return Colors.lightGreenAccent;
      case CovidColors.yellow:
        return Colors.yellowAccent;
      case CovidColors.orange:
        return Colors.orangeAccent;
      case CovidColors.purple:
        return Colors.purpleAccent;
      default:
        return Colors.white;
    }
  }
}
