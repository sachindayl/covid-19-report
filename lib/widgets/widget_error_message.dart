import 'package:flutter/material.dart';

class ErrorMessageWidget extends StatelessWidget {
  final String errorMessage;

  ErrorMessageWidget({this.errorMessage});

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(top: 20.0),
      child: Column(
        children: <Widget>[
          Icon(
            Icons.error_outline,
            color: Colors.red,
            size: 40,
          ),
          Padding(
            padding: const EdgeInsets.only(top: 16),
            child: Text(
              errorMessage,
              textAlign: TextAlign.center,
              style: TextStyle(
                  fontSize:
                  Theme.of(context).primaryTextTheme.bodyText1.fontSize),
            ),
          )
        ],
      ),
    );
  }
}
