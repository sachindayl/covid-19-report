import 'package:flutter/material.dart';

class HorizontalCompactListBuilderWidget extends StatelessWidget {
  final height;
  final list;
  final Function itemBuilder;
  final bool isScrollable;

  HorizontalCompactListBuilderWidget({this.height = double.infinity, this.list, this.itemBuilder, this.isScrollable = true});

  @override
  Widget build(BuildContext context) {
    return Container(
      height: height,
      margin: EdgeInsets.symmetric(horizontal: 10.0),
      child: ListView.builder(
        physics: isScrollable ? AlwaysScrollableScrollPhysics() : NeverScrollableScrollPhysics(),
        padding: EdgeInsets.symmetric(vertical: 10.0),
        itemBuilder: itemBuilder,
        itemCount: list.length,
      ),
    );
  }
}
