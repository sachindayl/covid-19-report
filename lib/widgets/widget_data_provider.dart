import 'package:flutter/material.dart';

class DataProviderWidget extends StatelessWidget {
  final bool isLocal;

  DataProviderWidget({this.isLocal});

  @override
  Widget build(BuildContext context) {
    return isLocal
        ? Container(
            padding: EdgeInsets.all(5.0),
            child: Text(
              'Data provided by Health Promotion Bureau in Sri Lanka',
              style: TextStyle(fontSize: 11.0, color: Colors.black54),
            ),
          )
        : Container(
            padding: EdgeInsets.all(5.0),
            child: Text(
              'Real-time data provided by Worldometers.info / NovelCovid API',
              style: TextStyle(fontSize: 11.0, color: Colors.black54),
            ),
          );
  }
}
