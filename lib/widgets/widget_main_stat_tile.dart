import 'package:flutter/material.dart';

import '../constants/_config_constants.dart';
import '../models/enum_covid_colors.dart';

class MainStatTileWidget extends StatelessWidget {
  final String title;
  final String count;
  final CovidColors countColor;

  MainStatTileWidget({this.title, this.count, this.countColor});

  @override
  Widget build(BuildContext context) {
    return Card(
        elevation: CARD_ELEVATION,
        color: Colors.white,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10),
        ),
        child: Container(
          margin: EdgeInsets.all(5.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Expanded(
                child: Container(
                  width: 150.0,
                  color: Colors.white,
                  alignment: Alignment.center,
                  child: Text(
                    title,
                    textAlign: TextAlign.center,
                    style: TextStyle(
                        color: Colors.black87,
                        fontSize: 20.0,
                        fontWeight: FontWeight.bold),
                  ),
                ),
              ),
              Expanded(
                child: Container(
                  width: 150.0,
                  alignment: Alignment.center,
                  decoration: BoxDecoration(
                      color: Colors.indigoAccent,
                      borderRadius: BorderRadius.circular(5)),
                  child: Stack(
                    children: <Widget>[
                      Text(
                        count,
                        textAlign: TextAlign.center,
                        style: TextStyle(
                          foreground: Paint()
                            ..style = PaintingStyle.stroke
                            ..strokeWidth = 0.8
                            ..color = Colors.white,
                          fontWeight: FontWeight.bold,
                          fontSize: 24.0,
                        ),
                      ),
                      Text(
                        count,
                        textAlign: TextAlign.center,
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                          color: getColor(this.countColor),
                          fontSize: 24.0,
                        ),
                      )
                    ],
                  ),
                ),
              )
            ],
          ),
        ));
  }

  Color getColor(CovidColors color) {
    switch (color) {
      case CovidColors.red:
        return Colors.redAccent;
      case CovidColors.green:
        return Colors.lightGreenAccent;
      case CovidColors.yellow:
        return Colors.yellowAccent;
      case CovidColors.orange:
        return Colors.orangeAccent;
      case CovidColors.purple:
        return Colors.purpleAccent;
      default:
        return Colors.white;
    }
  }
}
