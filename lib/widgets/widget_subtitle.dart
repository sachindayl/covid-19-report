import 'package:flutter/material.dart';

class SubTitleWidget extends StatelessWidget {
  final String title;
  final bool isViewAllEnabled;
  final Function onTap;

  SubTitleWidget({this.title, this.isViewAllEnabled = false, this.onTap});

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      padding: EdgeInsets.symmetric(vertical: 10.0, horizontal: 20.0),
      alignment: Alignment.centerLeft,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Flexible(
            flex: 2,
            child: Text(
              title,
              textAlign: TextAlign.left,
              style: TextStyle(
                  fontSize: 20.0, fontWeight: FontWeight.bold, color: Colors.black54),
            ),
          ),
          isViewAllEnabled ? Flexible(
            flex: 1,
            child: FlatButton(
              child: (Text('View All', style: TextStyle(fontSize: 16.0, color: Colors.indigoAccent),)),
              onPressed: onTap,
            ),
          ) : Container()
        ],
      ),
    );
  }
}
