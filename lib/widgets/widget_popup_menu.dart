import 'package:flutter/material.dart';

class PopUpMenuWidget extends StatelessWidget {
  final Function onSelected;

  PopUpMenuWidget(this.onSelected);
  @override
  Widget build(BuildContext context) {
    return PopupMenuButton<int>(
      onSelected: onSelected,
      itemBuilder: (BuildContext context) => <PopupMenuEntry<int>>[
        const PopupMenuItem<int>(
          value: 1,
          child: Text('About'),
        ),
      ],
    );
  }
}