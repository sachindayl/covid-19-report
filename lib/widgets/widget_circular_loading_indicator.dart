import 'package:flutter/material.dart';

class CircularProgressIndicatorWidget extends StatelessWidget {
  final bool isLoading;

  CircularProgressIndicatorWidget({this.isLoading = true});

  @override
  Widget build(BuildContext context) {
    return isLoading
        ? Container(
      margin: EdgeInsets.only(top: 20.0),
      child: SizedBox(
        child: CircularProgressIndicator(
          backgroundColor: Theme.of(context).accentColor,
          valueColor: AlwaysStoppedAnimation<Color>(Colors.purpleAccent),
        ),
        width: 40,
        height: 40,
      ),
    )
        : Container();
  }
}
