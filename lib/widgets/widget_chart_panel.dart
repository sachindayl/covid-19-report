import 'package:charts_flutter/flutter.dart' as charts;
import 'package:covidreport/constants/_config_constants.dart';
import 'package:covidreport/widgets/widget_subtitle.dart';
import 'package:flutter/material.dart';

class ChartPanelWidget extends StatelessWidget {
  final List<charts.Series> seriesList;
  final bool animate;
  final String title;

  ChartPanelWidget(this.seriesList, this.animate, this.title);

  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: CARD_ELEVATION,
      color: Colors.white,
      child: Container(
          child: Column(
            children: <Widget>[
              SubTitleWidget(title: title),
              seriesList.length > 0
                  ? Container(
                padding: EdgeInsets.all(10.0),
                width: MediaQuery.of(context).size.width,
                height: MediaQuery.of(context).size.height * 0.40,
                child: charts.TimeSeriesChart(
                  seriesList,
                  behaviors: [
                    new charts.SeriesLegend(position: charts.BehaviorPosition.bottom),
                  ],
                  animate: animate,
                  // Optionally pass in a [DateTimeFactory] used by the chart. The factory
                  // should create the same type of [DateTime] as the data provided. If none
                  // specified, the default creates local date time.
                  dateTimeFactory: const charts.LocalDateTimeFactory(),
                ),
              )
                  : Container(
                padding: EdgeInsets.all(5.0),
                child: Text('Loading...'),)
            ],
          )),
    );
  }

}
