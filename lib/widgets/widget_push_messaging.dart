import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';

import '../constants/_route_constants.dart' as Route;

class PushMessagingWidget extends StatefulWidget {
  @override
  State createState() => _PushMessagingWidgetState();
}

class _PushMessagingWidgetState extends State<PushMessagingWidget> {
  final FirebaseMessaging _fcm = FirebaseMessaging();

  @override
  Widget build(BuildContext context) {
    return Container();
  }

  @override
  void initState() {
    super.initState();
    _fcm.configure(
      onMessage: (Map<String, dynamic> message) async {
        print("onMessage: $message");
        final snackBar = SnackBar(
          content: Text(message['notification']['body']),
          action: SnackBarAction(
            label: 'OK',
            onPressed: () => null,
          ),
        );
        Scaffold.of(context).showSnackBar(snackBar);
      },
      onLaunch: (Map<String, dynamic> message) async {
        print("onLaunch: $message");
        Navigator.pushReplacementNamed(context, Route.ROOT);
      },
      onResume: (Map<String, dynamic> message) async {
        print("onResume: $message");
        // TODO optional
      },
    );
    _fcm.getToken().then((value) => print(value));
  }
}
