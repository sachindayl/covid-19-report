import 'package:flutter/material.dart';

import '../constants/_config_constants.dart';

class LocationListTileWidget extends StatelessWidget {
  final String image;
  final String title;
  final String count;
  final Function onTapFunction;

  LocationListTileWidget(
      this.image, this.title, this.count, this.onTapFunction);

  @override
  Widget build(BuildContext context) {
    return Card(
      color: Colors.white,
      elevation: CARD_ELEVATION,
      child: Container(
        margin: EdgeInsets.all(10.0),
        child: ListTile(
          leading: getImage(),
          title: Text(
            title,
            style: TextStyle(color: Colors.black87, fontSize: 20.0),
          ),
          trailing: Container(
            padding: EdgeInsets.all(10.0),
            decoration: BoxDecoration(
              color: Colors.indigoAccent,
              shape: BoxShape.rectangle,
              borderRadius: BorderRadius.all(Radius.elliptical(10, 10)),
            ),
            child: Text(
              count,
              style: TextStyle(
                  color: Colors.white,
                  fontSize: 24.0,
                  fontWeight: FontWeight.bold),
            ),
          ),
          onTap: onTapFunction,
        ),
      ),
    );
  }

  Widget getImage() {
    if (this.image != null) {
      return Container(
        padding: EdgeInsets.all(5.0),
        child: CircleAvatar(
          backgroundImage: NetworkImage(this.image),
        )
      );
    } else {
      return null;
    }
  }
}
