import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class LoadingIndicatorWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
        child: LinearProgressIndicator(
      backgroundColor: Colors.indigoAccent,
    ));
  }
}
