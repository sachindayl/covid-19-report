

class WorldModel {
  int cases;
  int todayCases;
  int deaths;
  int todayDeaths;
  int recovered;
  int active;
  int critical;
  num casesPerOneMillion;
  num deathsPerOneMillion;
  int tests;
  num testsPerOneMillion;
  int affectedCountries;

  WorldModel(
      this.cases,
      this.todayCases,
      this.deaths,
      this.todayDeaths,
      this.recovered,
      this.active,
      this.critical,
      this.casesPerOneMillion,
      this.deathsPerOneMillion,
      this.tests,
      this.testsPerOneMillion,
      this.affectedCountries);

  factory WorldModel.fromJson(Map<String, dynamic> json) {
    return WorldModel(
        json['cases'],
        json['todayCases'],
        json['deaths'],
        json['todayDeaths'],
        json['recovered'],
        json['active'],
        json['critical'],
        json['casesPerOneMillion'] != null ? json['casesPerOneMillion'] : 0,
        json['deathsPerOneMillion'] != null ? json['deathsPerOneMillion'] : 0,
        json['tests'],
        json['testsPerOneMillion'] != null ? json['testsPerOneMillion'] : 0,
        json['affectedCountries']
    );
  }
}
