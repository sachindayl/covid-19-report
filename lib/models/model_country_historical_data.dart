import 'package:covidreport/models/model_country_historical_timeline.dart';

class CountryHistoricalDataModel {
  CountryHistoricalTimelineModel timeline;

  CountryHistoricalDataModel({this.timeline});

  factory CountryHistoricalDataModel.fromJson(Map<String, dynamic> json) {
    return CountryHistoricalDataModel(
      timeline: CountryHistoricalTimelineModel.fromJson(json['timeline']),
    );
  }
}
