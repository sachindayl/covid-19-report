class DateTimeModel {
  String localDateTime;
  String internationalDateTime;

  DateTimeModel(this.localDateTime, this.internationalDateTime);
}
