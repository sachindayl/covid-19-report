import 'enum_covid_colors.dart';

class MainStatsModel {
  String title;
  String count;
  CovidColors covidColor;

  MainStatsModel(this.title, this.count, this.covidColor);
}
