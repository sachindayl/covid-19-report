import 'package:covidreport/models/model_time_series_cases.dart';

class TimeSeriesDataModel {
  final List<TimeSeriesCasesModel> confirmed;
  final List<TimeSeriesCasesModel> recovered;
  final List<TimeSeriesCasesModel> deaths;

  TimeSeriesDataModel(this.confirmed, this.recovered, this.deaths);
}