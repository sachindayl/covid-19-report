import 'package:covidreport/models/model_country_info.dart';

class CountryModel {
  String country;
  int cases;
  int todayCases;
  int deaths;
  int todayDeaths;
  int recovered;
  int active;
  int critical;
  num casesPerOneMillion;
  num deathsPerOneMillion;
  CountryInfoModel countryInfo;
  int tests;
  int testsPerOneMillion;

  CountryModel(
      this.country, this.cases, this.todayCases, this.deaths, this.todayDeaths, this.recovered, this.active, this.critical, this.casesPerOneMillion, this.deathsPerOneMillion, this.countryInfo, this.tests, this.testsPerOneMillion);

  factory CountryModel.fromJson(Map<String, dynamic> json) {
    return CountryModel(
      json['country'],
      json['cases'],
      json['todayCases'],
      json['deaths'],
      json['todayDeaths'],
      json['recovered'],
      json['active'],
      json['critical'],
      json['casesPerOneMillion'] != null ? json['casesPerOneMillion'] : 0,
      json['deathsPerOneMillion'] != null ? json['deathsPerOneMillion'] : 0,
      CountryInfoModel.fromJson(json['countryInfo']),
      json['tests'],
      json['testsPerOneMillion']
    );
  }
}
