

class CountryInfoModel {
  String flag;

  CountryInfoModel(this.flag);

  factory CountryInfoModel.fromJson(Map<String, dynamic> json) {
    return CountryInfoModel(
      json['flag'] != null ? json['flag'] : '',
    );
  }
}
