import 'model_data.dart';

class CovidStatsModel {
  final bool success;
  final String message;
  final DataModel data;

  CovidStatsModel({this.success, this.message, this.data});

  factory CovidStatsModel.fromJson(Map<String, dynamic> json) {
    return CovidStatsModel(
        success: json['success'],
        message: json['message'],
        data: DataModel.fromJson(json['data']));
  }
}
