import 'model_hospital.dart';

class HospitalDataModel {
  int id;
  int hospitalId;
  int cumulativeLocal;
  int cumulativeForeign;
  int treatmentLocal;
  int treatmentForeign;
  String createdAt;
  String updateAt;
  String deletedAt;
  int cumulativeTotal;
  int treatmentTotal;
  HospitalModel hospital;

  HospitalDataModel(
      this.id,
      this.hospitalId,
      this.cumulativeLocal,
      this.cumulativeForeign,
      this.treatmentLocal,
      this.treatmentForeign,
      this.createdAt,
      this.updateAt,
      this.deletedAt,
      this.cumulativeTotal,
      this.treatmentTotal,
      this.hospital);

  factory HospitalDataModel.fromJson(Map<String, dynamic> json) {
    return HospitalDataModel(
      json['id'],
      json['hospital_id'],
      json['cumulative_local'],
      json['cumulative_foreign'],
      json['treatment_local'],
      json['treatment_foreign'],
      json['created_at'] != null ? json['created_at'] : "",
      json['update_at'] != null ? json['update_at'] : "",
      json['deleted_at'] != null ? json['deleted_at'] : "",
      json['cumulative_total'],
      json['treatment_total'],
      HospitalModel.fromJson(json['hospital']),
    );
  }
}
