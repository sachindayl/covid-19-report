class TimeSeriesCasesModel {
  final DateTime time;
  final int cases;

  TimeSeriesCasesModel(this.time, this.cases);
}