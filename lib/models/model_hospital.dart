class HospitalModel {
  int id;
  String name;
  String createdAt;
  String updatedAt;
  String deletedAt;

  HospitalModel(
      this.id, this.name, this.createdAt, this.updatedAt, this.deletedAt);

  factory HospitalModel.fromJson(Map<String, dynamic> json) {
    return HospitalModel(
      json['id'],
      json['name'],
      json['created_at'] != null ? json['created_at'] : "",
      json['updated_at'] != null ? json['updated_at'] : "",
      json['deleted_at'] != null ? json['deleted_at'] : "",
    );
  }
}
