import 'package:covidreport/models/model_hospital_data.dart';

class DataModel {
  String updateDateTime;
  int localNewCases;
  int localTotalCases;
  int localTotalInHospitals;
  int localDeaths;
  int localNewDeaths;
  int localRecovered;
  int localActiveCases;
  int globalNewCases;
  int globalTotalCases;
  int globalDeaths;
  int globalNewDeaths;
  int globalRecovered;
  List<HospitalDataModel> hospitalData;

  DataModel(
      this.updateDateTime,
      this.localNewCases,
      this.localTotalCases,
      this.localTotalInHospitals,
      this.localDeaths,
      this.localNewDeaths,
      this.localRecovered,
      this.localActiveCases,
      this.globalNewCases,
      this.globalTotalCases,
      this.globalDeaths,
      this.globalNewDeaths,
      this.globalRecovered,
      this.hospitalData);

  factory DataModel.fromJson(Map<String, dynamic> json) {
    Iterable iterable = json['hospital_data'];
    List<HospitalDataModel> hospitalDataModelList =
        iterable.map((model) => HospitalDataModel.fromJson(model)).toList();
    return DataModel(
        json['update_date_time'],
        json['local_new_cases'],
        json['local_total_cases'],
        json['local_total_number_of_individuals_in_hospitals'],
        json['local_deaths'],
        json['local_new_deaths'],
        json['local_recovered'],
        json['local_active_cases'],
        json['global_new_cases'],
        json['global_total_cases'],
        json['global_deaths'],
        json['global_new_deaths'],
        json['global_recovered'],
        hospitalDataModelList);
  }
}
