

class CountryHistoricalTimelineModel {
  Map<String, dynamic> cases;
  Map<String, dynamic> deaths;
  Map<String, dynamic> recovered;

  CountryHistoricalTimelineModel(
      this.cases, this.deaths, this.recovered);

  factory CountryHistoricalTimelineModel.fromJson(Map<String, dynamic> json) {
    return CountryHistoricalTimelineModel(
      json['cases'],
      json['deaths'],
      json['recovered'],
    );
  }
}
