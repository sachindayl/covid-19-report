const String ROOT = '/';
const String HOSPITAL_DETAILS = '/hospital_details';
const String LOCATION_DETAILS = '/location_details';
const String AUTHOR = '/author';
const String HOSPITAL_LISTING = '/hospital_listing';
const String INTERNATIONAL_LISTING = '/international_listing';
