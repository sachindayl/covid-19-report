import 'dart:convert';

import 'package:http/http.dart' as http;

import '../models/model_covid_stats.dart';

class HealthPromoService {
  http.Client _httpClient;

  HealthPromoService() {
    _httpClient = new http.Client();
  }

  Future<CovidStatsModel> fetchCovidLocalStats() async {
    final String url = 'http://healthpromo.gov.lk/api/get-current-statistical';
    final http.Response response = await _httpClient.get(url,
        headers: {'Content-Type': 'application/json'}).catchError((err) {
      throw err;
    });
    if (response.statusCode == 200) {
      return CovidStatsModel.fromJson(jsonDecode(response.body));
    } else {
      throw Exception('failed to retrieve covid stats');
    }
  }
}
