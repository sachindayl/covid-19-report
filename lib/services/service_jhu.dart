import 'dart:convert';

import 'package:covidreport/models/model_country_historical_data.dart';
import 'package:covidreport/models/model_world.dart';
import 'package:http/http.dart' as http;

import '../constants/_config_constants.dart' as Config;
import '../models/model_country.dart';

class JHUService {
  http.Client _httpClient;

  JHUService() {
    _httpClient = http.Client();
  }

  Future<List<CountryModel>> fetchCountriesData() async {
    final String url = 'https://corona.lmao.ninja/v2/countries?sort=country';
    final http.Response response =
        await _httpClient.get(url, headers: Config.headers).catchError((err) {
      throw err;
    });
    if (response.statusCode == 200) {
      Iterable iterable = jsonDecode(response.body);
      List<CountryModel> countries =
          iterable.map((model) => CountryModel.fromJson(model)).toList();
      return countries;
    } else {
      throw Exception('failed to retrieve all countries stats');
    }
  }

  Future<CountryModel> fetchCountryData(String country) async {
    final String url = 'https://corona.lmao.ninja/v2/countries/$country';
    final http.Response response =
        await _httpClient.get(url, headers: Config.headers).catchError((err) {
      throw err;
    });
    if (response.statusCode == 200) {
      return CountryModel.fromJson(jsonDecode(response.body));
    } else {
      throw Exception('failed to retrieve country stats');
    }
  }

  Future<WorldModel> fetchWorldStats() async {
    final String url = 'https://corona.lmao.ninja/v2/all';
    final http.Response response =
        await _httpClient.get(url, headers: Config.headers).catchError((err) {
      throw err;
    });
    if (response.statusCode == 200) {
      return WorldModel.fromJson(jsonDecode(response.body));
    } else {
      throw Exception('failed to retrieve world stats');
    }
  }

  Future<CountryHistoricalDataModel> fetchHistoricalStats(
      String country) async {
    final String url = 'https://corona.lmao.ninja/v2/historical/$country';
    final http.Response response =
        await _httpClient.get(url, headers: Config.headers).catchError((err) {
      throw err;
    });
    if (response.statusCode == 200) {
      return CountryHistoricalDataModel.fromJson(jsonDecode(response.body));
    } else {
      throw Exception('failed to retrieve historic data stats');
    }
  }
}
