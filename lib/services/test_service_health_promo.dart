import '../models/model_covid_stats.dart';

class HealthPromoTestService {
  HealthPromoTestService();

  Future<CovidStatsModel> fetchCovidLocalStats() async {
    var sampleJson = {
      "success": true,
      "message": "Success",
      "data": {
        "update_date_time": "2020-03-17 08:14:26",
        "local_new_cases": 10,
        "local_total_cases": 29,
        "local_deaths": 0,
        "local_recovered": 1,
        "global_new_cases": 13903,
        "global_total_cases": 167511,
        "global_deaths": 6606,
        "global_recovered": 862,
        "hospital_data": [
          {
            "id": 1,
            "hospital_id": 1,
            "cumulative_local": 10,
            "cumulative_foreign": 10,
            "treatment_local": 10,
            "treatment_foreign": 10,
            "created_at": "2020-03-17 08:14:26",
            "updated_at": null,
            "deleted_at": null,
            "cumulative_total": 20,
            "treatment_total": 20,
            "hospital": {
              "id": 1,
              "name": "National Institute of Infectious Diseases",
              "created_at": "2020-03-17 08:11:42",
              "updated_at": "2020-03-17 08:11:42",
              "deleted_at": null
            }
          },
          {
            "id": 2,
            "hospital_id": 2,
            "cumulative_local": 10,
            "cumulative_foreign": 10,
            "treatment_local": 10,
            "treatment_foreign": 10,
            "created_at": "2020-03-17 08:14:26",
            "updated_at": null,
            "deleted_at": null,
            "cumulative_total": 50,
            "treatment_total": 20,
            "hospital": {
              "id": 2,
              "name": "National Hospital Sri Lanka",
              "created_at": "2020-03-17 08:11:42",
              "updated_at": "2020-03-17 08:11:42",
              "deleted_at": null
            }
          },
          {
            "id": 3,
            "hospital_id": 3,
            "cumulative_local": 21,
            "cumulative_foreign": 2,
            "treatment_local": 13,
            "treatment_foreign": 0,
            "created_at": "2020-03-18 12:38:49",
            "updated_at": null,
            "deleted_at": null,
            "cumulative_total": 23,
            "treatment_total": 13,
            "hospital": {
              "id": 3,
              "name": "TH - Ragama",
              "created_at": "2020-03-17 13:34:15",
              "updated_at": "2020-03-17 13:34:15",
              "deleted_at": null
            }
          },
          {
            "id": 4,
            "hospital_id": 4,
            "cumulative_local": 71,
            "cumulative_foreign": 11,
            "treatment_local": 3,
            "treatment_foreign": 1,
            "created_at": "2020-03-18 12:38:49",
            "updated_at": null,
            "deleted_at": null,
            "cumulative_total": 82,
            "treatment_total": 4,
            "hospital": {
              "id": 4,
              "name": "TH - Karapitiya",
              "created_at": "2020-03-17 13:34:15",
              "updated_at": "2020-03-17 13:34:15",
              "deleted_at": null
            }
          },
          {
            "id": 5,
            "hospital_id": 5,
            "cumulative_local": 17,
            "cumulative_foreign": 2,
            "treatment_local": 8,
            "treatment_foreign": 0,
            "created_at": "2020-03-18 12:38:49",
            "updated_at": null,
            "deleted_at": null,
            "cumulative_total": 19,
            "treatment_total": 8,
            "hospital": {
              "id": 5,
              "name": "TH - Anuradhapura",
              "created_at": "2020-03-17 13:34:15",
              "updated_at": "2020-03-17 13:34:15",
              "deleted_at": null
            }
          },
        ]
      }
    };
    return CovidStatsModel.fromJson(sampleJson);
  }
}
