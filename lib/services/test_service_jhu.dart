import 'package:covidreport/models/model_country_historical_data.dart';
import 'package:covidreport/models/model_world.dart';

import '../models/model_country.dart';

class JHUTestService {
  JHUTestService();

  Future<List<CountryModel>> fetchCountryStats() async {
    var sampleJson = [
      {
        "country": "Zambia",
        "countryInfo": {
          "_id": 894,
          "iso2": "ZM",
          "iso3": "ZMB",
          "lat": -15,
          "long": 30,
          "flag":
          "https://raw.githubusercontent.com/NovelCOVID/API/master/assets/flags/zm.png"
        },
        "cases": 39,
        "todayCases": 0,
        "deaths": 1,
        "todayDeaths": 0,
        "recovered": 2,
        "active": 36,
        "critical": 0,
        "casesPerOneMillion": 2,
        "deathsPerOneMillion": 0.05,
        "updated": 1586002527432
      },
      {
        "country": "World",
        "countryInfo": {
          "_id": null,
          "iso2": null,
          "iso3": null,
          "lat": 0,
          "long": 0,
          "flag": "https://raw.githubusercontent.com/NovelCOVID/API/master/assets/flags/unknow.png"
        },
        "cases": 1138646,
        "todayCases": 22003,
        "deaths": 61142,
        "todayDeaths": 1984,
        "recovered": 236185,
        "active": 841319,
        "critical": 39664,
        "casesPerOneMillion": 146,
        "deathsPerOneMillion": 7.8,
        "updated": 1586007327217
      },
      {
        "country": "Turks and Caicos Islands",
        "countryInfo": {
          "_id": 796,
          "iso2": "TC",
          "iso3": "TCA",
          "lat": 21.75,
          "long": -71.5833,
          "flag": "https://raw.githubusercontent.com/NovelCOVID/API/master/assets/flags/tc.png"
        },
        "cases": 5,
        "todayCases": 0,
        "deaths": 0,
        "todayDeaths": 0,
        "recovered": 0,
        "active": 5,
        "critical": 0,
        "casesPerOneMillion": 129,
        "deathsPerOneMillion": null,
        "updated": 1586007327238
      },
    ];

    List<CountryModel> countries = sampleJson
        .map((model) => CountryModel.fromJson(model))
        .toList();
    return countries;
  }

  Future<WorldModel> fetchWorldStats() async {
    var sampleJson = {
      "updated": 1587131876401,
      "cases": 2206676,
      "todayCases": 25368,
      "deaths": 148663,
      "todayDeaths": 3192,
      "recovered": 558440,
      "active": 1499573,
      "critical": 56482,
      "casesPerOneMillion": 283,
      "deathsPerOneMillion": 19,
      "tests": 18578903,
      "testsPerOneMillion": 2382.7,
      "affectedCountries": 212
    };
    return WorldModel.fromJson(sampleJson);
  }

  Future<CountryHistoricalDataModel> fetchHistoricalStats() async {
    var sampleJson = {
      "country": "Canada",
      "provinces": [
        "alberta",
        "british columbia",
        "grand princess",
        "manitoba",
        "new brunswick",
        "newfoundland and labrador",
        "nova scotia",
        "ontario",
        "prince edward island",
        "quebec",
        "saskatchewan",
        "diamond princess",
        "recovered",
        "northwest territories",
        "yukon"
      ],
      "timeline": {
        "cases": {
          "3/6/20": 49,
          "3/7/20": 54,
          "3/8/20": 64,
          "3/9/20": 77,
          "3/10/20": 79,
          "3/11/20": 108,
          "3/12/20": 117,
          "3/13/20": 193,
          "3/14/20": 198,
          "3/15/20": 252,
          "3/16/20": 415,
          "3/17/20": 478,
          "3/18/20": 657,
          "3/19/20": 800,
          "3/20/20": 943,
          "3/21/20": 1277,
          "3/22/20": 1469,
          "3/23/20": 2088,
          "3/24/20": 2790,
          "3/25/20": 3251,
          "3/26/20": 4042,
          "3/27/20": 4682,
          "3/28/20": 5576,
          "3/29/20": 6280,
          "3/30/20": 7398,
          "3/31/20": 8527,
          "4/1/20": 9560,
          "4/2/20": 11284,
          "4/3/20": 12437,
          "4/4/20": 12978
        },
        "deaths": {
          "3/6/20": 0,
          "3/7/20": 0,
          "3/8/20": 0,
          "3/9/20": 1,
          "3/10/20": 1,
          "3/11/20": 1,
          "3/12/20": 1,
          "3/13/20": 1,
          "3/14/20": 1,
          "3/15/20": 1,
          "3/16/20": 4,
          "3/17/20": 5,
          "3/18/20": 8,
          "3/19/20": 9,
          "3/20/20": 12,
          "3/21/20": 19,
          "3/22/20": 21,
          "3/23/20": 25,
          "3/24/20": 26,
          "3/25/20": 30,
          "3/26/20": 38,
          "3/27/20": 54,
          "3/28/20": 61,
          "3/29/20": 64,
          "3/30/20": 80,
          "3/31/20": 101,
          "4/1/20": 109,
          "4/2/20": 139,
          "4/3/20": 179,
          "4/4/20": 218
        },
        "recovered": {
          "3/6/20": 0,
          "3/7/20": 0,
          "3/8/20": 0,
          "3/9/20": 0,
          "3/10/20": 0,
          "3/11/20": 0,
          "3/12/20": 0,
          "3/13/20": 0,
          "3/14/20": 0,
          "3/15/20": 0,
          "3/16/20": 0,
          "3/17/20": 0,
          "3/18/20": 0,
          "3/19/20": 0,
          "3/20/20": 0,
          "3/21/20": 0,
          "3/22/20": 0,
          "3/23/20": 0,
          "3/24/20": 0,
          "3/25/20": 0,
          "3/26/20": 0,
          "3/27/20": 0,
          "3/28/20": 0,
          "3/29/20": 0,
          "3/30/20": 0,
          "3/31/20": 0,
          "4/1/20": 0,
          "4/2/20": 0,
          "4/3/20": 0,
          "4/4/20": 0
        }
      }
    };
    return CountryHistoricalDataModel.fromJson(sampleJson);
  }


}
