import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:covidreport/models/model_country_historical_data.dart';

class Covid19Service {
  Covid19Service();

  Future<CountryHistoricalDataModel> fetchSriLankanHistoricStats() async {
    var data = Firestore.instance
        .collection('covid_data')
        .document('historic_data')
        .get()
        .then((value) => value.data);
    var historicalData = await data.then((value) => value);
    return CountryHistoricalDataModel.fromJson(historicalData);
  }
}
