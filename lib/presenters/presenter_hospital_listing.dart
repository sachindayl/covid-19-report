import 'package:covidreport/models/model_covid_stats.dart';
import 'package:covidreport/models/model_hospital.dart';
import 'package:covidreport/models/model_hospital_data.dart';
import 'package:covidreport/services/service_health_promo.dart';

class HospitalListingView {

}

class HospitalListingPresenter {
  // ignore: missing_return
  Future<List<HospitalDataModel>> retrieveHospitalStats() async {}
}

class HospitalListingPresenterImpl implements HospitalListingPresenter {
  HealthPromoService _healthPromoService;

  HospitalListingPresenterImpl() {
    this._healthPromoService = HealthPromoService();
  }

  @override
  Future<List<HospitalDataModel>> retrieveHospitalStats() async{
    CovidStatsModel covidStatsModel = await _healthPromoService.fetchCovidLocalStats();
    return covidStatsModel.data.hospitalData;
  }
}
