import 'dart:async';
import 'dart:io';

import 'package:covidreport/models/model_time_series_cases.dart';
import 'package:covidreport/models/model_world.dart';
import 'package:flutter/foundation.dart' as Foundation;
import 'package:intl/intl.dart';

import '../config/analytics_screen.dart';
import '../models/enum_covid_colors.dart';
import '../models/model_country.dart';
import '../models/model_title_panel.dart';
import '../services/service_jhu.dart';
import '../services/test_service_jhu.dart';

class CoronaStatsView {
  void setTime(String dateTime) {}

  void setMainStatsLists(List<MainStatsModel> internationalStatsList) async {}

  void showCountryStats(List<CountryModel> countryStats) {}

  void showLoadingIndicator() {}

  void hideLoadingIndicator() {}
}

class CoronaStatsPresenter {
  set coronaStatsView(CoronaStatsView view) {}

  void getCovidStats() async {}

  void addMainStats(WorldModel world) {}

  void setAnalyticsScreen() {}

  void dispose() {}
}

class CoronaStatsPresenterImpl
    with AnalyticsScreen
    implements CoronaStatsPresenter {
  CoronaStatsView _view;
  JHUService _jhuService;
  JHUTestService _jhuTestService;
  List<MainStatsModel> _internationalStatsList = [];
  WorldModel _world;

  CoronaStatsPresenterImpl() {
    this._jhuService = JHUService();
    this._jhuTestService = JHUTestService();
  }

  @override
  set coronaStatsView(CoronaStatsView view) {
    this._view = view;
  }

  @override
  void setAnalyticsScreen() {
    setCurrentScreen('Main stats', 'Stats class');
  }

  @override
  void getCovidStats() async {
    try {
      this._view.showLoadingIndicator();

      List<CountryModel> countryStats;
      if (Foundation.kReleaseMode) {
        countryStats = await this._jhuService.fetchCountriesData();
        _world = await this._jhuService.fetchWorldStats();
      } else {
        countryStats = await this._jhuTestService.fetchCountryStats();
        _world = await this._jhuTestService.fetchWorldStats();
      }
      countryStats.sort((a, b) => b.cases.compareTo(a.cases));
      List<CountryModel> countrySubStats = countryStats.sublist(0, 3);

      addMainStats(_world);

      DateTime now = DateTime.now();
      DateFormat formatter = DateFormat('yyyy-MM-dd HH:mm:ss');
      this._view.setTime(formatter.format(now));

      this._view.setMainStatsLists(_internationalStatsList);
      this._view.showCountryStats(countrySubStats);
      this._view.hideLoadingIndicator();
    } on TimeoutException catch (tError) {
      print(tError.message);
    } on SocketException catch (sError) {
      print(sError.message);
    }
  }

  @override
  void addMainStats(WorldModel world) {
    _internationalStatsList.clear();

    _internationalStatsList.add(MainStatsModel('Confirmed new cases',
        world.todayCases.toString(), CovidColors.yellow));
    _internationalStatsList.add(MainStatsModel(
        'Confirmed total cases', world.cases.toString(), CovidColors.yellow));
    _internationalStatsList.add(MainStatsModel(
        'Confirmed active cases', world.active.toString(), CovidColors.orange));
    _internationalStatsList.add(MainStatsModel('Confirmed critical cases',
        world.critical.toString(), CovidColors.orange));
    _internationalStatsList.add(MainStatsModel(
        'Total recovered', world.recovered.toString(), CovidColors.green));
    _internationalStatsList.add(MainStatsModel(
        'Total new deaths', world.todayDeaths.toString(), CovidColors.red));
    _internationalStatsList.add(MainStatsModel(
        'Total deaths', world.deaths.toString(), CovidColors.red));
    _internationalStatsList.add(MainStatsModel('Cases per one million',
        world.casesPerOneMillion.toString(), CovidColors.yellow));
    _internationalStatsList.add(MainStatsModel('Deaths per one million',
        world.deathsPerOneMillion.toString(), CovidColors.red));
    _internationalStatsList.add(MainStatsModel('Tested',
        world.tests.toString(), CovidColors.purple));
    _internationalStatsList.add(MainStatsModel('Tests per one million',
        world.testsPerOneMillion.toString(), CovidColors.purple));
    _internationalStatsList.add(MainStatsModel('Affected countries',
        world.affectedCountries.toString(), CovidColors.purple));
  }

  List<TimeSeriesCasesModel> timeSeriesBuilder(Map<String, int> cases) {
    List<TimeSeriesCasesModel> timeSeriesCases = [];
    cases.forEach((key, value) {
      var date = DateFormat('dd/MM/yyyy').parse(key);
      timeSeriesCases.add(TimeSeriesCasesModel(
          DateTime(date.year, date.month, date.day), value));
    });
    return timeSeriesCases;
  }

  @override
  void dispose() {
    this._view = null;
    this._jhuService = null;
    this._jhuTestService = null;
  }
}
