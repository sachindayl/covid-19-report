import 'dart:async';
import 'dart:io';

import 'package:covidreport/config/time_series_builder.dart';
import 'package:flutter/foundation.dart' as Foundation;

import '../config/analytics_screen.dart';
import '../models/model_country_historical_data.dart';
import '../models/model_time_series_data.dart';
import '../services/service_jhu.dart';
import '../services/test_service_jhu.dart';

class LocationProfileView {
  void showHistoricalData(TimeSeriesDataModel data) {}

  void showLoadingIndicator() {}

  void hideLoadingIndicator() {}
}

class LocationProfilePresenter {
  set locationProfileView(LocationProfileView view) {}

  void getHistoricData(String country) async {}

  void setAnalyticsScreen() {}

  void dispose() {}
}

class LocationProfilePresenterImpl
    with AnalyticsScreen
    implements LocationProfilePresenter {
  LocationProfileView _view;
  JHUService _jhuService;
  JHUTestService _jhuTestService;
  CountryHistoricalDataModel _historicalData;
  TimeSeriesBuilder _timeSeriesBuilder;

  LocationProfilePresenterImpl() {
    this._jhuService = JHUService();
    this._jhuTestService = JHUTestService();
  }

  @override
  set locationProfileView(LocationProfileView view) {
    this._view = view;
  }

  @override
  void setAnalyticsScreen() {
    setCurrentScreen('Location stats', 'Stats class');
  }

  void getHistoricData(String country) async {
    _timeSeriesBuilder = TimeSeriesBuilder();
    try {
      this._view.showLoadingIndicator();
      if (Foundation.kReleaseMode) {
        _historicalData = await this
            ._jhuService
            .fetchHistoricalStats(country)
            .timeout(const Duration(seconds: 5));
      } else {
        _historicalData = await this._jhuTestService.fetchHistoricalStats();
      }
      var confirmed =
          _timeSeriesBuilder.buildTimeSeries(_historicalData.timeline.cases);
      var recovered = _timeSeriesBuilder
          .buildTimeSeries(_historicalData.timeline.recovered);
      var deaths =
          _timeSeriesBuilder.buildTimeSeries(_historicalData.timeline.deaths);
      this._view.showHistoricalData(
          TimeSeriesDataModel(confirmed, recovered, deaths));
      this._view.hideLoadingIndicator();
    } on TimeoutException catch (err) {
      print(err.message);
      this._view.hideLoadingIndicator();
    } on SocketException catch (err) {
      print(err.message);
      this._view.hideLoadingIndicator();
    }
  }

  @override
  void dispose() {
    this._view = null;
    this._jhuService = null;
    this._jhuTestService = null;
  }
}
