import 'package:covidreport/models/model_country.dart';
import 'package:covidreport/services/service_jhu.dart';

class InternationalListingView {

}

class InternationalListingPresenter {

  // ignore: missing_return
  Future<List<CountryModel>> retrieveInternationalStats() async {}
}

class InternationalListingPresenterImpl implements InternationalListingPresenter {
  JHUService _jhuService;

  InternationalListingPresenterImpl(){
    _jhuService = JHUService();
  }
  @override
  Future<List<CountryModel>> retrieveInternationalStats() async {
    var internationalStats = await _jhuService.fetchCountriesData();
    internationalStats.sort((a, b) => a.country.compareTo(b.country));
    return internationalStats;
  }


}