import 'dart:async';
import 'dart:io';

import 'package:covidreport/config/time_series_builder.dart';
import 'package:covidreport/models/enum_covid_colors.dart';
import 'package:covidreport/models/model_country.dart';
import 'package:covidreport/models/model_covid_stats.dart';
import 'package:covidreport/models/model_hospital_data.dart';
import 'package:covidreport/models/model_title_panel.dart';
import 'package:covidreport/services/service_covid19.dart';
import 'package:covidreport/services/service_health_promo.dart';
import 'package:covidreport/services/service_jhu.dart';
import 'package:covidreport/services/test_service_health_promo.dart';
import 'package:covidreport/services/test_service_jhu.dart';
import 'package:flutter/foundation.dart' as Foundation;

import '../config/analytics_screen.dart';
import '../models/model_country_historical_data.dart';
import '../models/model_time_series_data.dart';

class SriLankanStatsView {
  void setTime(String dateTime) {}

  void showHistoricalData(TimeSeriesDataModel data) {}

  void showLoadingIndicator() {}

  void showHospitalList(List<HospitalDataModel> hospitalData) {}

  void setMainStatsLists(List<MainStatsModel> localStatsList) async {}

  void hideLoadingIndicator() {}
}

class SriLankanStatsPresenter {
  set sriLankanStatsView(SriLankanStatsView view) {}

  void getMainStats() async {}

  void getHistoricData() async {}

  void addMainStats(CovidStatsModel covidStats, CountryModel lankanIntlStats) {}

  void setAnalyticsScreen() {}

  void dispose() {}
}

class SriLankanStatsPresenterImpl
    with AnalyticsScreen
    implements SriLankanStatsPresenter {
  SriLankanStatsView _view;
  HealthPromoService _healthPromoService;
  HealthPromoTestService _healthPromoTestService;
  JHUService _jhuService;
  JHUTestService _jhuTestService;
  Covid19Service _covid19Service;
  TimeSeriesBuilder _timeSeriesBuilder;
  CountryHistoricalDataModel _sriLankanHistoricalData;
  List<MainStatsModel> _localStatsList = [];
  CovidStatsModel _stats;
  CountryModel _countryModel;

  SriLankanStatsPresenterImpl() {
    if (Foundation.kReleaseMode) {
      this._healthPromoService = HealthPromoService();
      this._jhuService = JHUService();
    } else {
      this._healthPromoTestService = HealthPromoTestService();
      this._jhuService = JHUService();
//      this._jhuTestService = JHUTestService();
    }
    this._covid19Service = Covid19Service();
    _timeSeriesBuilder = TimeSeriesBuilder();
  }

  @override
  set sriLankanStatsView(SriLankanStatsView view) {
    this._view = view;
  }

  @override
  void setAnalyticsScreen() {
    setCurrentScreen('Sri Lankan stats', 'Stats class');
  }

  @override
  void getMainStats() async {
    try {
      if (Foundation.kReleaseMode) {
        _stats = await this._healthPromoService.fetchCovidLocalStats();
        _countryModel = await this._jhuService.fetchCountryData('LK');
      } else {
        _stats = await this._healthPromoTestService.fetchCovidLocalStats();
        _countryModel = await this._jhuService.fetchCountryData('LK');
      }
      addMainStats(_stats, _countryModel);
      this._view.setTime(_stats.data.updateDateTime);
      this._view.setMainStatsLists(_localStatsList);

      List<HospitalDataModel> hospitalData = _stats.data.hospitalData;
      hospitalData.sort((a, b) => b.treatmentTotal.compareTo(a.treatmentTotal));
      List<HospitalDataModel> hospitalSubData = hospitalData.sublist(0, 3);

      this._view.showHospitalList(hospitalSubData);
    } on TimeoutException catch (tError) {
      print(tError.message);
    } on SocketException catch (sError) {
      print(sError.message);
    }
  }

  @override
  void addMainStats(CovidStatsModel covidStats, CountryModel lankanIntlStats) {
    _localStatsList.clear();
    _localStatsList.add(MainStatsModel('New positive cases',
        covidStats.data.localNewCases.toString(), CovidColors.yellow));
    _localStatsList.add(MainStatsModel('Total positive cases',
        covidStats.data.localTotalCases.toString(), CovidColors.yellow));
    _localStatsList.add(MainStatsModel('Total active cases',
        covidStats.data.localActiveCases.toString(), CovidColors.orange));
    _localStatsList.add(MainStatsModel('Total in hospitals',
        covidStats.data.localTotalInHospitals.toString(), CovidColors.orange));
    _localStatsList.add(MainStatsModel('Recovered cases',
        covidStats.data.localRecovered.toString(), CovidColors.green));
    _localStatsList.add(MainStatsModel('New deaths',
        covidStats.data.localNewDeaths.toString(), CovidColors.red));
    _localStatsList.add(MainStatsModel('Total deaths',
        covidStats.data.localDeaths.toString(), CovidColors.red));
    _localStatsList.add(MainStatsModel('Tests',
        lankanIntlStats.tests.toString(), CovidColors.purple));
    _localStatsList.add(MainStatsModel('Tests per one million',
        lankanIntlStats.testsPerOneMillion.toString(), CovidColors.purple));
  }

  @override
  void getHistoricData() async {
    try {
      _sriLankanHistoricalData =
          await this._covid19Service.fetchSriLankanHistoricStats();
      var cases = _timeSeriesBuilder
          .buildTimeSeries(_sriLankanHistoricalData.timeline.cases);
      var recovered = _timeSeriesBuilder
          .buildTimeSeries(_sriLankanHistoricalData.timeline.recovered);
      var deaths = _timeSeriesBuilder
          .buildTimeSeries(_sriLankanHistoricalData.timeline.deaths);
      this
          ._view
          .showHistoricalData(TimeSeriesDataModel(cases, recovered, deaths));
    } on TimeoutException catch (err) {
      print(err.message);
    } on SocketException catch (err) {
      print(err.message);
    }
  }

  @override
  void dispose() {
    this._view = null;
    this._healthPromoService = null;
    this._healthPromoTestService = null;
    this._covid19Service = null;
  }
}
