import 'package:covidreport/presenters/presenter_hospital_listing.dart';
import 'package:covidreport/presenters/presenter_international_listing.dart';
import 'package:covidreport/presenters/presenter_location_profile.dart';
import 'package:covidreport/screens/screen_hospitals_listing.dart';
import 'package:covidreport/screens/screen_international_listing.dart';
import 'package:flutter/material.dart';

import './constants/_route_constants.dart' as Route;
import './presenters/presenter_corona_stats.dart';
import './screens/screen_author.dart';
import './screens/screen_corona_stats.dart';
import './screens/screen_hospital_profile.dart';
import './screens/screen_location_profile.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Covid-19 Report',
      theme: ThemeData(primarySwatch: Colors.indigo, fontFamily: 'Baloo2'),
      initialRoute: Route.ROOT,
      routes: {
        Route.ROOT: (context) => CoronaStatsScreen(CoronaStatsPresenterImpl()),
        Route.HOSPITAL_DETAILS: (context) => HospitalProfileScreen(),
        Route.LOCATION_DETAILS: (context) => LocationProfileScreen(LocationProfilePresenterImpl()),
        Route.AUTHOR: (context) => AuthorScreen(),
        Route.HOSPITAL_LISTING: (context) => HospitalListingScreen(HospitalListingPresenterImpl()),
        Route.INTERNATIONAL_LISTING: (context) => InternationalListingScreen(InternationalListingPresenterImpl())
      },
    );
  }
}
