import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:flutter/foundation.dart' as Foundation;

abstract class AnalyticsScreen {
  void setCurrentScreen(String screen, String screenClass) async {
    if (Foundation.kReleaseMode) {
      await FirebaseAnalytics()
          .setCurrentScreen(
              screenName: screen, screenClassOverride: screenClass)
          .then((_) => print('current screen set to: $screen'));
    }
  }
}
