import 'package:covidreport/models/model_time_series_cases.dart';

class TimeSeriesBuilder {
  List<TimeSeriesCasesModel> buildTimeSeries(Map<String, dynamic> cases) {
    List<TimeSeriesCasesModel> timeSeriesCases = [];
    var casesPointOffset = cases.length ~/ 30;
    var casesPointOffsetDoubleMod = cases.length % 30;
    if(casesPointOffsetDoubleMod >= 10) {
      casesPointOffset = casesPointOffset + 1;
    }
    var keyPoints = (cases.length < 30) ? cases.length : 30;

//    String key;
//    var dateItems;
//    var day;
//    var month;
//    var year;

    try{
      for (int i = casesPointOffset; i < keyPoints; i++) {
        String key = cases.keys.elementAt(i);
        var dateItems = key.split('/');
        var day = int.parse(dateItems[1]);
        var month = int.parse(dateItems[0]);
        var year = int.parse(dateItems[2]);
        var casesAmount = cases.values.elementAt(i) as int;
        var date = DateTime(year, month, day);
        timeSeriesCases.add(TimeSeriesCasesModel(date, casesAmount));
        i = i + casesPointOffset;
      }
    } on FormatException catch(error) {
      print(error.message);
    }

//    timeSeriesCases.add(TimeSeriesCasesModel())
    timeSeriesCases.sort((a, b) => a.time.compareTo(b.time));
    return timeSeriesCases;
  }
}
