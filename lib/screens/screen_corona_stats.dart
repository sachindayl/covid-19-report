import 'package:covidreport/presenters/presenter_sri_lankan_stats.dart';
import 'package:covidreport/screens/screen_sri_lankan_stats.dart';
import 'package:covidreport/widgets/widget_loading_indicator.dart';
import 'package:flutter/material.dart';

import '../config/analytics_screen.dart';
import '../constants/_route_constants.dart' as Route;
import '../models/model_country.dart';
import '../models/model_title_panel.dart';
import '../presenters/presenter_corona_stats.dart';
import '../widgets/widget_data_provider.dart';
import '../widgets/widget_horizontal_list_view_builder.dart';
import '../widgets/widget_location_list_tile.dart';
import '../widgets/widget_main_stat_tile.dart';
import '../widgets/widget_popup_menu.dart';
import '../widgets/widget_push_messaging.dart';
import '../widgets/widget_subtitle.dart';
import '../widgets/widget_title_panel.dart';

class CoronaStatsScreen extends StatefulWidget {
  final CoronaStatsPresenter _presenter;

  CoronaStatsScreen(this._presenter);

  @override
  State createState() => _CoronaStatsScreenState();
}

class _CoronaStatsScreenState extends State<CoronaStatsScreen>
    with AnalyticsScreen
    implements CoronaStatsView {
  final List<Tab> tabs = <Tab>[
    Tab(icon: Icon(Icons.home)),
    Tab(icon: Icon(Icons.language))
  ];

  List<String> _internationalTime = [];

  List<MainStatsModel> _internationalStatsTiles = [];

  List<CountryModel> _countryStats = [];
  bool _isLoading = false;

  _CoronaStatsScreenState();

  @override
  void initState() {
    super.initState();
    this.widget._presenter.coronaStatsView = this;
    this.widget._presenter.setAnalyticsScreen();
    this.widget._presenter.getCovidStats();
  }

  @override
  void dispose() {
    super.dispose();
    this.widget._presenter.dispose();
  }

  @override
  Widget build(BuildContext context) {
    PushMessagingWidget();
    return DefaultTabController(
      length: this.tabs.length,
      child: Scaffold(
        appBar: AppBar(
          title: Text('COVID-19'),
          centerTitle: true,
          bottom: TabBar(
            tabs: this.tabs,
          ),
          actions: <Widget>[
            IconButton(
              icon: const Icon(
                Icons.info_outline,
                color: Colors.white,
              ),
              onPressed: () => Navigator.pushNamed(context, Route.AUTHOR),
            )
          ],
        ),
        body: TabBarView(
          children: <Widget>[
            SriLankanStatsScreen(),
            SingleChildScrollView(
              child: Container(
                margin: EdgeInsets.only(top: 5.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    this._isLoading
                        ? LoadingIndicatorWidget()
                        : Container(
                            padding: EdgeInsets.symmetric(vertical: 2.0),
                          ),
                    TitlePanelWidget('International ', _internationalTime),
                    internationalMainStatsPanel(),
                    _isLoading
                        ? Container()
                        : SubTitleWidget(
                            title: 'Confirmed cases worldwide',
                            isViewAllEnabled: true,
                            onTap: () => Navigator.of(context).pushNamed(Route.INTERNATIONAL_LISTING),
                          ),
                    internationalLocationStatsPanel(),
                    DataProviderWidget(
                      isLocal: false,
                    )
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget internationalMainStatsPanel() => Container(
      height: 150.0,
      child: ListView.builder(
        scrollDirection: Axis.horizontal,
        itemBuilder: (context, index) => MainStatTileWidget(
          title: _internationalStatsTiles[index].title,
          count: _internationalStatsTiles[index].count,
          countColor: _internationalStatsTiles[index].covidColor,
        ),
        itemCount: _internationalStatsTiles.length,
        padding: EdgeInsets.symmetric(vertical: 5.0),
      ));

  Widget internationalLocationStatsPanel() =>
      HorizontalCompactListBuilderWidget(
        isScrollable: false,
        height: 300.0,
        list: _countryStats,
        itemBuilder: (context, index) {
          var countryDetails = _countryStats[index];
          if (countryDetails.country == "Sri Lanka" ||
              countryDetails.country == "World") {
            return Container();
          } else {
            return LocationListTileWidget(
              countryDetails.countryInfo.flag,
              countryDetails.country,
              countryDetails.cases.toString(),
              () {
                gotoLocationProfile(context, countryDetails);
              },
            );
          }
        },
      );

  void gotoLocationProfile(BuildContext context, CountryModel country) {
    Navigator.pushNamed(context, Route.LOCATION_DETAILS, arguments: country);
  }

  @override
  void setTime(String dateTime) {
    setState(() {
      this._internationalTime = dateTime.split(" ");
    });
  }

  @override
  void setMainStatsLists(List<MainStatsModel> internationalStatsList) async {
    this._internationalStatsTiles.clear();
    setState(() {
      _internationalStatsTiles.addAll(internationalStatsList);
    });
  }

  @override
  void showCountryStats(List<CountryModel> countryStats) {
    _countryStats.clear();
    setState(() {
      _countryStats.addAll(countryStats);
    });
  }

  @override
  void showLoadingIndicator() {
    setState(() {
      this._isLoading = true;
    });
  }

  @override
  void hideLoadingIndicator() {
    setState(() {
      this._isLoading = false;
    });
  }
}
