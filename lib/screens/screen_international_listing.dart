import 'package:covidreport/models/model_country.dart';
import 'package:covidreport/presenters/presenter_international_listing.dart';
import 'package:covidreport/widgets/widget_circular_loading_indicator.dart';
import 'package:covidreport/widgets/widget_error_message.dart';
import 'package:covidreport/widgets/widget_horizontal_list_view_builder.dart';
import 'package:covidreport/widgets/widget_location_list_tile.dart';
import 'package:covidreport/widgets/widget_popup_menu.dart';
import 'package:flutter/material.dart';
import '../config/analytics_screen.dart';
import '../constants/_route_constants.dart' as Route;

class InternationalListingScreen extends StatelessWidget with AnalyticsScreen {
  final InternationalListingPresenter _presenter;
  InternationalListingScreen(this._presenter);
  @override
  Widget build(BuildContext context) {
    setCurrentScreen('Hospital listing', 'Stats class');
    return Scaffold(
      appBar: AppBar(
        title: Text('COVID-19'),
        centerTitle: true,
        actions: <Widget>[
          IconButton(
            icon: const Icon(
              Icons.info_outline,
              color: Colors.white,
            ),
            onPressed: () => Navigator.pushNamed(context, Route.AUTHOR),
          )
        ],
      ),
      body: Container(
        width: double.infinity,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            getCountriesList()
          ],
        ),
      ),
    );
  }

  Widget getCountriesList() {
    return FutureBuilder<List<CountryModel>>(
      future: _presenter.retrieveInternationalStats(),
      builder: (context, snapshot) {
        if (snapshot.hasData) {
          return Flexible(
            flex: 1,
            child: HorizontalCompactListBuilderWidget(
              height: MediaQuery.of(context).size.height,
              list: snapshot.data,
              itemBuilder: (context, index) {
                return LocationListTileWidget(
                    snapshot.data[index].countryInfo.flag,
                    snapshot.data[index].country,
                    snapshot.data[index].cases.toString(), () {
                  Navigator.pushNamed(context, Route.LOCATION_DETAILS,
                      arguments: snapshot.data[index]);
                });
              },
            ),
          );
        } else if (snapshot.hasError) {
          print(snapshot.error);
          return ErrorMessageWidget(
              errorMessage: 'Connection failed. Please try again later.');
        } else {
          return CircularProgressIndicatorWidget();
        }
      },
    );
  }
}
