import 'package:covidreport/config/analytics_screen.dart';
import 'package:flutter/material.dart';

import '../constants/_config_constants.dart';
import '../constants/_route_constants.dart' as Route;
import '../models/model_hospital_data.dart';
import '../widgets/widget_data_provider.dart';
import '../widgets/widget_popup_menu.dart';
import '../widgets/widget_profile_item.dart';
import '../widgets/widget_subtitle.dart';

class HospitalProfileScreen extends StatefulWidget {
  HospitalProfileScreen();

  @override
  State createState() => _HospitalProfileScreenState();
}

class _HospitalProfileScreenState extends State<HospitalProfileScreen>
    with AnalyticsScreen {
  HospitalDataModel hospitalData;

  @override
  Widget build(BuildContext context) {
    hospitalData = ModalRoute.of(context).settings.arguments;
    setCurrentScreen(
        'Hospital profile: ${hospitalData.hospital.name}', 'Stats class');
    return Scaffold(
      appBar: AppBar(
        title: Text('COVID-19'),
        centerTitle: true,
        actions: <Widget>[
          IconButton(
            icon: const Icon(
              Icons.info_outline,
              color: Colors.white,
            ),
            onPressed: () => Navigator.pushNamed(context, Route.AUTHOR),
          )
        ],
      ),
      body: SingleChildScrollView(
        child: Container(
          margin: EdgeInsets.symmetric(horizontal: 20.0),
          child: Column(
            children: <Widget>[
              Container(
                padding: EdgeInsets.all(10.0),
                child: Text(
                  hospitalData.hospital.name,
                  style: TextStyle(fontSize: 24.0, fontWeight: FontWeight.bold),
                  textAlign: TextAlign.center,
                ),
              ),
              Container(
                child: currentStats(),
              ),
              Container(
                child: cumulativeStats(),
              ),
              DataProviderWidget(
                isLocal: true,
              )
            ],
          ),
        ),
      ),
    );
  }

  Widget currentStats() => Card(
      elevation: CARD_ELEVATION,
      color: Colors.white,
      child: Container(
        child: Column(
          children: <Widget>[
            SubTitleWidget(title: 'Currently on treatment'),
            ProfileItemWidget(
                title: 'Local', count: hospitalData.treatmentLocal.toString()),
            ProfileItemWidget(
                title: 'Foreign',
                count: hospitalData.treatmentForeign.toString()),
            ProfileItemWidget(
                title: 'Total', count: hospitalData.treatmentTotal.toString()),
          ],
        ),
      ));

  Widget cumulativeStats() => Card(
      color: Colors.white,
      elevation: CARD_ELEVATION,
      child: Container(
        child: Column(
          children: <Widget>[
            SubTitleWidget(title: 'Overall count treated/observed'),
            ProfileItemWidget(
                title: 'local', count: hospitalData.cumulativeLocal.toString()),
            ProfileItemWidget(
                title: 'Foreign',
                count: hospitalData.cumulativeForeign.toString()),
            ProfileItemWidget(
                title: 'Total', count: hospitalData.cumulativeTotal.toString()),
          ],
        ),
      ));
}
