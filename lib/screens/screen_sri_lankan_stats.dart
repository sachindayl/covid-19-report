import 'package:charts_flutter/flutter.dart' as charts;
import 'package:covidreport/config/analytics_screen.dart';
import 'package:covidreport/constants/_route_constants.dart' as Route;
import 'package:covidreport/models/model_hospital_data.dart';
import 'package:covidreport/models/model_time_series_cases.dart';
import 'package:covidreport/models/model_time_series_data.dart';
import 'package:covidreport/models/model_title_panel.dart';
import 'package:covidreport/presenters/presenter_sri_lankan_stats.dart';
import 'package:covidreport/widgets/widget_chart_panel.dart';
import 'package:covidreport/widgets/widget_data_provider.dart';
import 'package:covidreport/widgets/widget_horizontal_list_view_builder.dart';
import 'package:covidreport/widgets/widget_loading_indicator.dart';
import 'package:covidreport/widgets/widget_location_list_tile.dart';
import 'package:covidreport/widgets/widget_main_stat_tile.dart';
import 'package:covidreport/widgets/widget_subtitle.dart';
import 'package:covidreport/widgets/widget_title_panel.dart';
import 'package:flutter/cupertino.dart';

class SriLankanStatsScreen extends StatefulWidget {
  SriLankanStatsScreen();

  @override
  State createState() => _SriLankanStatsScreenState();
}

class _SriLankanStatsScreenState extends State<SriLankanStatsScreen>
    with AnalyticsScreen
    implements SriLankanStatsView {
  SriLankanStatsPresenter _presenter;
  bool _isLoading = false;
  List<String> _localTime = [];
  List<HospitalDataModel> _hospitalDataList = [];
  List<MainStatsModel> _localStatsTiles = [];
  List<charts.Series<TimeSeriesCasesModel, DateTime>> _historicDataList = [];

  _SriLankanStatsScreenState();

  @override
  void initState() {
    super.initState();
    this._presenter = SriLankanStatsPresenterImpl();
    this._presenter.sriLankanStatsView = this;
    this._presenter.setAnalyticsScreen();
    this._presenter.getMainStats();
    this._presenter.getHistoricData();
  }

  @override
  void dispose() {
    super.dispose();
    this._presenter.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Container(
        margin: EdgeInsets.only(top: 5.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            _isLoading
                ? LoadingIndicatorWidget()
                : Container(
                    padding: EdgeInsets.symmetric(vertical: 1.0),
                  ),
            TitlePanelWidget('Sri Lankan ', this._localTime),
            localMainStatsPanel(),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 10.0, vertical: 5.9),
              child: ChartPanelWidget(
                  _historicDataList, false, 'Total cases timeline'),
            ),
            _isLoading
                ? Container()
                : SubTitleWidget(
                    title: 'Individuals currently getting treatment',
                    isViewAllEnabled: true,
                    onTap: () =>
                        Navigator.pushNamed(context, Route.HOSPITAL_LISTING),
                  ),
            hospitalStatsPanel(context),
            DataProviderWidget(
              isLocal: true,
            )
          ],
        ),
      ),
    );
  }

  @override
  void showHospitalList(List<HospitalDataModel> hospitalData) {
    _hospitalDataList.clear();
    setState(() {
      _hospitalDataList.addAll(hospitalData);
    });
  }

  @override
  void showHistoricalData(TimeSeriesDataModel data) {
    if (data != null) {
      setState(() {
        _historicDataList = [
          charts.Series<TimeSeriesCasesModel, DateTime>(
            id: 'Confirmed',
            colorFn: (_, __) => charts.MaterialPalette.yellow.shadeDefault,
            domainFn: (TimeSeriesCasesModel cases, _) => cases.time,
            measureFn: (TimeSeriesCasesModel cases, _) => cases.cases,
            data: data.confirmed,
          ),
          charts.Series<TimeSeriesCasesModel, DateTime>(
            id: 'Recovered',
            colorFn: (_, __) => charts.MaterialPalette.green.shadeDefault,
            domainFn: (TimeSeriesCasesModel cases, _) => cases.time,
            measureFn: (TimeSeriesCasesModel cases, _) => cases.cases,
            data: data.recovered,
          ),
          charts.Series<TimeSeriesCasesModel, DateTime>(
            id: 'Deaths',
            colorFn: (_, __) => charts.MaterialPalette.red.shadeDefault,
            domainFn: (TimeSeriesCasesModel cases, _) => cases.time,
            measureFn: (TimeSeriesCasesModel cases, _) => cases.cases,
            data: data.deaths,
          )
        ];
      });
    } else {
      _historicDataList = [];
    }
  }

  Widget localMainStatsPanel() => Container(
      height: 150.0,
      child: ListView.builder(
        scrollDirection: Axis.horizontal,
        itemBuilder: (context, index) => MainStatTileWidget(
          title: _localStatsTiles[index].title,
          count: _localStatsTiles[index].count,
          countColor: _localStatsTiles[index].covidColor,
        ),
        itemCount: _localStatsTiles.length,
        padding: EdgeInsets.symmetric(vertical: 5.0),
      ));

  Widget hospitalStatsPanel(BuildContext context) =>
      HorizontalCompactListBuilderWidget(
        height: 300.0,
        list: _hospitalDataList,
        isScrollable: false,
        itemBuilder: (context, index) {
          return LocationListTileWidget(
              null,
              _hospitalDataList[index].hospital.name,
              _hospitalDataList[index].treatmentTotal.toString(), () {
            Navigator.pushNamed(context, Route.HOSPITAL_DETAILS,
                arguments: _hospitalDataList[index]);
          });
        },
      );

  @override
  void setMainStatsLists(List<MainStatsModel> localStatsList) async {
    this._localStatsTiles.clear();
    setState(() {
      _localStatsTiles.addAll(localStatsList);
    });
  }

  @override
  void setTime(String dateTime) {
    setState(() {
      this._localTime = dateTime.split(" ");
    });
  }

  @override
  void showLoadingIndicator() {
    setState(() {
      this._isLoading = true;
    });
  }

  @override
  void hideLoadingIndicator() {
    setState(() {
      this._isLoading = false;
    });
  }
}
