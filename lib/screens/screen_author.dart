import 'package:flutter/material.dart';
import 'package:flutter_linkify/flutter_linkify.dart';
import 'package:url_launcher/url_launcher.dart';

import '../config/analytics_screen.dart';
import '../constants/_config_constants.dart';
import '../widgets/widget_subtitle.dart';

class AuthorScreen extends StatelessWidget with AnalyticsScreen {
  @override
  Widget build(BuildContext context) {
    setCurrentScreen('Author profile', 'Settings class');
    return Scaffold(
      appBar: AppBar(
        title: Text('COVID-19'),
        centerTitle: true,
      ),
      body: Container(
        margin: EdgeInsets.all(20.0),
        child: Card(
          elevation: CARD_ELEVATION,
          child: Container(
            height: 300.0,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                SubTitleWidget(title: 'Developed by'),
                Container(
                  child: Text(
                    'Sachinda Liyanaarachchi',
                    style: TextStyle(fontSize: 24.0),
                  ),
                ),
                Divider(
                  indent: 20.0,
                  thickness: 2.0,
                  endIndent: 20.0,
                  color: Colors.indigoAccent,
                ),
                Text('Stay safe. Keep the distance.'),
                Text('Together, we will get through this.'),
                Container(
                  margin: EdgeInsets.only(top: 20.0),
                  child: Text('Find application updates on my twitter Account:'),
                ),
                Linkify(
                    onOpen: (link) async {
                      if (await canLaunch(link.url)) {
                        await launch(link.url);
                      } else {
                        throw 'Could not launch $link';
                      }
                    },
                    text: "https://twitter.com/Sachinda27"),
                  Text('version: 1.0.8'),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
