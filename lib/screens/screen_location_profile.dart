import 'package:charts_flutter/flutter.dart' as charts;
import 'package:flutter/material.dart';

import '../config/analytics_screen.dart';
import '../constants/_config_constants.dart';
import '../constants/_route_constants.dart' as Route;
import '../models/enum_covid_colors.dart';
import '../models/model_country.dart';
import '../models/model_time_series_cases.dart';
import '../models/model_time_series_data.dart';
import '../presenters/presenter_location_profile.dart';
import '../widgets/widget_chart_panel.dart';
import '../widgets/widget_data_provider.dart';
import '../widgets/widget_popup_menu.dart';
import '../widgets/widget_profile_item.dart';
import '../widgets/widget_subtitle.dart';

class LocationProfileScreen extends StatefulWidget {
  final LocationProfilePresenter _presenter;

  LocationProfileScreen(this._presenter);

  @override
  State createState() => _LocationProfileScreenState();
}

class _LocationProfileScreenState extends State<LocationProfileScreen>
    with AnalyticsScreen
    implements LocationProfileView {
  CountryModel countryModel;
  bool _historicDataLoaded = false;
  bool _isLoading = false;
  List<charts.Series<TimeSeriesCasesModel, DateTime>> historicDataList = [];

  _LocationProfileScreenState();

  @override
  void initState() {
    super.initState();
    this.widget._presenter.locationProfileView = this;
    this.widget._presenter.setAnalyticsScreen();
  }

  @override
  void dispose() {
    super.dispose();
    this.widget._presenter.dispose();
  }

  @override
  Widget build(BuildContext context) {
    countryModel = ModalRoute.of(context).settings.arguments;
    loadHistoricData();
    setCurrentScreen(
        'Location Profile: ${countryModel.country}', 'Stats class');
    return Scaffold(
      appBar: AppBar(
        title: Text('COVID-19'),
        centerTitle: true,
        actions: <Widget>[
          IconButton(
            icon: const Icon(
              Icons.info_outline,
              color: Colors.white,
            ),
            onPressed: () => Navigator.pushNamed(context, Route.AUTHOR),
          )
        ],
      ),
      body: SingleChildScrollView(
        child: Container(
          margin: EdgeInsets.symmetric(horizontal: 20.0),
          child: Column(
            children: <Widget>[
              _isLoading
                  ? loadingIndicator()
                  : Container(
                      padding: EdgeInsets.symmetric(vertical: 1.0),
                    ),
              Center(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: <Widget>[
                    Container(
                      width: 50.0,
                    ),
                    Container(
                      padding: EdgeInsets.all(10.0),
                      child: ClipRect(
                        child: Image.network(
                          countryModel.countryInfo.flag,
                          fit: BoxFit.cover,
                          width: 80.0,
                          height: 40.0,
                        ),
                      ),
                    ),
                    Flexible(
                      flex: 1,
                      child: Container(
                        padding: EdgeInsets.all(10.0),
                        child: Text(
                          countryModel.country,
                          style: TextStyle(
                              fontSize: 24.0, fontWeight: FontWeight.bold),
                          textAlign: TextAlign.left,
                        ),
                      ),
                    ),
                    Container(
                      width: 50.0,
                    ),
                  ],
                ),
              ),
              ChartPanelWidget(historicDataList, false, 'Total cases timeline'),
              Container(
                child: currentStats(),
              ),
              DataProviderWidget(
                isLocal: false,
              )
            ],
          ),
        ),
      ),
    );
  }

  Widget currentStats() => Card(
      elevation: CARD_ELEVATION,
      color: Colors.white,
      child: Container(
        child: Column(
          children: <Widget>[
            SubTitleWidget(title: 'Current statistics'),
            ProfileItemWidget(
              title: 'New confirmed',
              count: countryModel.todayCases.toString(),
              countColor: CovidColors.yellow,
            ),
            ProfileItemWidget(
              title: 'Confirmed',
              count: countryModel.cases.toString(),
              countColor: CovidColors.yellow,
            ),
            ProfileItemWidget(
              title: 'Critical',
              count: countryModel.critical.toString(),
              countColor: CovidColors.orange,
            ),
            ProfileItemWidget(
              title: 'Active',
              count: countryModel.active.toString(),
              countColor: CovidColors.orange,
            ),
            ProfileItemWidget(
              title: 'Recovered',
              count: countryModel.recovered.toString(),
              countColor: CovidColors.green,
            ),
            ProfileItemWidget(
              title: 'New deaths',
              count: countryModel.todayDeaths.toString(),
              countColor: CovidColors.red,
            ),
            ProfileItemWidget(
              title: 'Deaths',
              count: countryModel.deaths.toString(),
              countColor: CovidColors.red,
            ),
            ProfileItemWidget(
              title: 'Cases per one million',
              count: countryModel.casesPerOneMillion.toString(),
              countColor: CovidColors.yellow,
            ),
            ProfileItemWidget(
              title: 'Deaths per one million',
              count: countryModel.deathsPerOneMillion.toString(),
              countColor: CovidColors.red,
            ),
            ProfileItemWidget(
              title: 'Tested',
              count: countryModel.tests.toString(),
              countColor: CovidColors.purple,
            ),
            ProfileItemWidget(
              title: 'Tests per one million',
              count: countryModel.testsPerOneMillion.toString(),
              countColor: CovidColors.purple,
            ),
          ],
        ),
      ));

  Widget loadingIndicator() => Container(
          child: LinearProgressIndicator(
        backgroundColor: Colors.indigoAccent,
      ));

  void loadHistoricData() {
    if (!_historicDataLoaded) {
      this.widget._presenter.getHistoricData(countryModel.country);
      _historicDataLoaded = true;
    }
  }

  @override
  void showHistoricalData(TimeSeriesDataModel data) {
    if (data != null) {
      historicDataList = [
        charts.Series<TimeSeriesCasesModel, DateTime>(
          id: 'Confirmed',
          colorFn: (_, __) => charts.MaterialPalette.yellow.shadeDefault,
          domainFn: (TimeSeriesCasesModel cases, _) => cases.time,
          measureFn: (TimeSeriesCasesModel cases, _) => cases.cases,
          data: data.confirmed,
        ),
        charts.Series<TimeSeriesCasesModel, DateTime>(
          id: 'Recovered',
          colorFn: (_, __) => charts.MaterialPalette.green.shadeDefault,
          domainFn: (TimeSeriesCasesModel cases, _) => cases.time,
          measureFn: (TimeSeriesCasesModel cases, _) => cases.cases,
          data: data.recovered,
        ),
        charts.Series<TimeSeriesCasesModel, DateTime>(
          id: 'Deaths',
          colorFn: (_, __) => charts.MaterialPalette.red.shadeDefault,
          domainFn: (TimeSeriesCasesModel cases, _) => cases.time,
          measureFn: (TimeSeriesCasesModel cases, _) => cases.cases,
          data: data.deaths,
        )
      ];
      setState(() {});
    } else {
      historicDataList = [];
    }
  }

  @override
  void showLoadingIndicator() {
    setState(() {
      this._isLoading = true;
    });
  }

  @override
  void hideLoadingIndicator() {
    setState(() {
      this._isLoading = false;
    });
  }
}
